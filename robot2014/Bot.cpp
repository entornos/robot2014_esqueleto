#include <Arduino.h>
#include <avr/io.h> 
#include <avr/wdt.h>
#include "Bot.h"
#include "PinChangeInt.h"

//////////////////////////////////////////////////////////////////////////
// Constantes privadas (no compartidas)
//////////////////////////////////////////////////////////////////////////

// TODO: convertir tacos a UNSIGNED LONG

// ENTRADAS analogicas
const static int SEN1 = 4;  // PC4
const static int SEN2 = 1;  // PC1
const static int SEN3 = 2;  // PC2
const static int SEN4 = 3;  // PC3
const static int SEN5 = 0;  // PC0
const static int SEN6 = 5;  // PC5
const static int BAT = 6;

// SALIDAS DIGITALES
const static int MIA = 6;   // PD6
const static int MIB = 5;   // PD5
const static int MDA = 11;  // PB3
const static int MDB = 3;   // PD3
const static int LED = 1;   // PD1
const static int LEDON = 7; // PD7
const static int TRIG = 2;

// ENTRADAS DIGITALES
const static int TACO_IZDO = 8;   // SEN7 = PB0
const static int TACO_DCHO = 10;  // SEN8 = PB2
const static int ECHO = 2;

// CONSTANTES
const static uint32_t MAX_SENSOR_START_TIME = 18000;  // uS
const static uint32_t MAX_SENSOR_DISTANCE = 500;
const static int US_ROUNDTRIP_CM = 57;  // uS to travel 1 cm roundtrip
const static uint32_t MAX_SENSOR_PING_TIME = MAX_SENSOR_DISTANCE * US_ROUNDTRIP_CM + (US_ROUNDTRIP_CM / 2);

// ESTADOS DEL SONAR
const static int SONAR_OFF = 0;
const static int WAIT_ECHO_CLEAR = 1;
const static int WAIT_PING_START = 2;
const static int WAIT_PING_END = 3;

//////////////////////////////////////////////////////////////////////////
// Variables privadas (no compartidas)
//////////////////////////////////////////////////////////////////////////

volatile static int gTacoIzdo = 0;
volatile static int gTacoDcho = 0;
static uint8_t triggerBit;
static uint8_t echoBit;
volatile static uint8_t *triggerOutput;
volatile static uint8_t *triggerMode;
volatile static uint8_t *echoInput;
volatile static uint32_t pingT0;
volatile static uint32_t pingTime = -1;
volatile static int estadoSonar = SONAR_OFF;
volatile static boolean nuevaDist = false;

//////////////////////////////////////////////////////////////////////////
// Funciones privadas (no compartidas)
//////////////////////////////////////////////////////////////////////////

static void intTacoIzdo()
{
  gTacoIzdo++;
}

static void intTacoDcho()
{
  gTacoDcho++;
}

static void intSonar()
{
  switch(estadoSonar) {
    case WAIT_ECHO_CLEAR:
      if (!(*echoInput & echoBit))
        estadoSonar = WAIT_PING_START;
      break;

    case WAIT_PING_START:
      if (*echoInput & echoBit) {
        estadoSonar = WAIT_PING_END;
        pingT0 = micros();
      }
      break;
      
    case WAIT_PING_END:
      if (!(*echoInput & echoBit)) {
        estadoSonar = SONAR_OFF;
        pingTime = micros() - pingT0;
        nuevaDist = true;
      }
      break;
  }
}

//////////////////////////////////////////////////////////////////////////
// Funciones públicas
//////////////////////////////////////////////////////////////////////////

void bot_setup()
{
  pinMode(MIA, OUTPUT);
  pinMode(MIB, OUTPUT);
  pinMode(MDA, OUTPUT);
  pinMode(MDB, OUTPUT);
  pinMode(LED, OUTPUT);
  pinMode(LEDON, OUTPUT);
  
  pinMode(TACO_IZDO, INPUT);
  pinMode(TACO_DCHO, INPUT);
  
  // interrupciones tacos
  PCintPort::attachInterrupt(TACO_IZDO, intTacoIzdo, CHANGE);
  PCintPort::attachInterrupt(TACO_DCHO, intTacoDcho, CHANGE);
  
  // pines / bits para el sonar
  triggerBit = digitalPinToBitMask(TRIG);  // port register bitmask trig pin
  echoBit = digitalPinToBitMask(ECHO);     // echo pin
  triggerOutput = portOutputRegister(digitalPinToPort(TRIG));
  echoInput = portInputRegister(digitalPinToPort(ECHO));
  triggerMode = (uint8_t *) portModeRegister(digitalPinToPort(TRIG));
  
  // interrupciones sonar
  PCintPort::attachInterrupt(ECHO, intSonar, CHANGE);
  
  digitalWrite(LED, LOW);    // Apaga el led rojo
  digitalWrite(LEDON, HIGH); // Enciende las luces de los sensores
}

int bot_lee_tecla()
{
  int b = -1;
  if (Serial.available() > 0) {
    b = Serial.read();
  }
  return b;
}

void bot_lee_sensores(int s[])
{
  s[0] = analogRead(SEN1);
  s[1] = analogRead(SEN2);
  s[2] = analogRead(SEN3);
  s[3] = analogRead(SEN4);
  s[4] = analogRead(SEN5);
  s[5] = analogRead(SEN6);
}

int bot_taco_izdo()
{
  // Copia los valores de los tacometros atomicamente
  cli();
  int t = gTacoIzdo;
  sei();
  return t;
}

int bot_taco_dcho()
{
  // Copia los valores de los tacometros atomicamente
  cli();
  int t = gTacoDcho;
  sei();
  return t;
}

void bot_velocidad(int izda, int dcha)
{
  // Motor izquierdo
  if (izda >  255) izda =  255;
  if (izda < -255) izda = -255;
  if (dcha >  255) dcha =  255;
  if (dcha < -255) dcha = -255;

  if (izda == 0) {
    digitalWrite(MIA, HIGH);
    digitalWrite(MIB, HIGH);
  } else if (izda > 0) {
    digitalWrite(MIA, HIGH);
    analogWrite(MIB, 255 - izda);
  } else {
    analogWrite(MIA, 255 + izda);
    digitalWrite(MIB, HIGH);
  }
  
  // Motor derecho
  if (dcha == 0) {
    digitalWrite(MDA, HIGH);
    digitalWrite(MDB, HIGH);
  } else if (dcha > 0) {
    digitalWrite(MDA, HIGH);
    analogWrite(MDB, 255 - dcha);
  } else {
    analogWrite(MDA, 255 + dcha);
    digitalWrite(MDB, HIGH);
  }
}

void bot_reset()
{
  wdt_enable(WDTO_1S); 
}

// Devuelve
int bot_bateria()
{
  unsigned int a = analogRead(BAT);
  unsigned long mv = a * 2UL * 5000 / 1024;
  return mv;
}

void bot_sonar_ping()
{
  int es;
  cli();
  es = estadoSonar;
  sei();
  if (es == SONAR_OFF) {
    pingTime = -1;
    *triggerMode |= triggerBit;      // Trigger pin to output
    *triggerOutput &= ~triggerBit;   // Trigger pin to low (should already be)  
    delayMicroseconds(4);            // Wait for pint to go low, testing show 4us is ok
    *triggerOutput |= triggerBit;    // Trigger pin high, tells the sensor to ping
    delayMicroseconds(10);           // Wait 10us for the sensor to ack the trigger
    estadoSonar = WAIT_ECHO_CLEAR;
    *triggerOutput &= ~triggerBit;   // Trigger pin low
    *triggerMode &= ~triggerBit;     // Trigger pin to input
  }
}

int bot_sonar_dist()
{
  uint32_t pt;
  cli();
  pt = pingTime;
  nuevaDist = false;
  sei();
  if (pt == -1)
    return pt;
  else
    return pt * 11 / 64;  // mm
}

boolean bot_nueva_dist()
{
  int nd;
  cli();
  nd = nuevaDist;
  sei();
  return nd;
}
