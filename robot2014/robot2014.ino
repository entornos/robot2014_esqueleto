#include "Bot.h"
#include <math.h>

// Constantes globales
const int PARAR = 0;
const int SIGUE_LINEAS_RAPIDO = 1;
const int PRUEBA_IZDA = 2;
const int PRUEBA_DCHA = 3;

// Variables globales
boolean muestra_sensores = false;
int estado = PARAR;    // maquina de estados
int s[6];
int ti0, td0;
int dist;

// Funciones
void manda_telemetria()
{
  int i;
  char cadena[64];
  for (i = 0; i < 6; i++) {
    sprintf(cadena, "%03d ", s[i]);
    Serial.print(cadena);
  }
  Serial.print(" d=");
  Serial.print(dist);
  Serial.print(" ti=");
  Serial.print(bot_taco_izdo());
  Serial.print(" td=");
  Serial.print(bot_taco_dcho());
  Serial.print(" Vbat=");
  Serial.println(bot_bateria());
  
}

void sigue_lineas() 
{
}

void pasa_a_estado(int nuevo_estado) {
  switch(nuevo_estado) {
    case PARAR:
      bot_velocidad(0, 0);
      break;
    case PRUEBA_IZDA:
      bot_velocidad(30, 0);
      break;
    case PRUEBA_DCHA:
      bot_velocidad(0, 30);
      break;
  }
  estado = nuevo_estado;
}

void durante_estado() {
  switch (estado) {
    case PARAR:
      break;
    case SIGUE_LINEAS_RAPIDO:
      sigue_lineas();
      break;
    case PRUEBA_IZDA: 
    case PRUEBA_DCHA:
      break;
  }  
}

void setup()
{
  bot_setup();
  bot_sonar_ping();
  Serial.begin(57600);
}

void loop()
{
  if (muestra_sensores) {
    bot_lee_sensores(s);
    if (bot_nueva_dist()) {
      dist = bot_sonar_dist();
      bot_sonar_ping();
    }
    manda_telemetria();
  }
    
  int tecla = bot_lee_tecla();
 
  switch (tecla) {
    case 'R':
      // Resetea y vuelve al bootloader
      Serial.end();
      bot_reset();
    case 'q':
    case ' ':
      pasa_a_estado(PARAR);
      break;
    case 's':
      pasa_a_estado(SIGUE_LINEAS_RAPIDO);
      break;
    case 'm':
      muestra_sensores = !muestra_sensores;
      break;
    case 'i':
      pasa_a_estado(PRUEBA_IZDA);
      break;
    case 'd':
      pasa_a_estado(PRUEBA_DCHA);
      break;
  }
  durante_estado();
}
