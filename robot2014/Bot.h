#ifndef __BOT_H__
#define __BOT_H__

#include <Arduino.h>

void bot_setup();
int bot_lee_tecla();
void bot_lee_sensores(int s[]);
int bot_taco_izdo();
int bot_taco_dcho();
void bot_velocidad(int izda, int dcha);
void bot_reset();
int bot_bateria();
void bot_sonar_ping();
int bot_sonar_dist();
boolean bot_nueva_dist();

#endif
