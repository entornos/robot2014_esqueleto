# Configuración Arduino

Para funcionar con Stino es necesario Arduino 1.5.8+, con 1.0.6 no encuentra el avrdude.

En boards.txt:

```
orangutan.name=Orangutan via bluetooth

orangutan.upload.protocol=arduino
orangutan.upload.maximum_size=30720
orangutan.upload.speed=57600
orangutan.upload.disable_flushing=true
orangutan.upload.tool=avrdude

orangutan.build.mcu=atmega328p
orangutan.build.f_cpu=20000000L
orangutan.build.core=arduino
orangutan.build.variant=standard
```

# Configuración bluetooth:

* Instalar blueman:
```
sudo apt-get install blueman
```

* Reiniciar la sesión

* En el icono de bluetooth -> Setup new device -> emparejar, pedirá la clave. Conectar a 'serial device'. La luz azul en el robot debería quedarse fija.

* Para desconectar/conectar: menú de inicio -> Configuración -> Administrador de bluetooth. Para conectar, botón derecho en el dispositivo -> Conectar a Dev B. Y para desconectar lo mismo.

* Si se apaga el robot sin desconectar el bluetooth, hay que eliminar el dispositivo y volver a emparejarlo.

# Instación de Stino:

* Bajar Stino (zip) de: https://github.com/Robot-Will/Stino

* Descomprimirlo en el directorio de packages del Sublime (Preferences -> Browse packages).

* En stino/pyarduino/base/serial_port.py:
```
    dev_names = ['ttyACM*', 'ttyUSB*', 'rfcomm*']
```

# Configuración de Sublime:

* Arduino -> Preferences -> Select arduino application folder

* Arduino -> Serial port -> rfcomm0

* Arduino -> Board -> Orangutan via bluetooth

# Subir código al robot:

* Mantener reset pulsado en el robot.

* En sublime: Arduino -> Upload (alt+a, alt+u). Cuando diga "Uploading", tenemos <1 seg para soltar el reset.
